\documentclass[]{beamer}
\usepackage{fontspec,polyglossia,xunicode,hyperref,csquotes}
\usepackage{graphicx}
\graphicspath{{img/}{}}
\usetheme[]{Boadilla}
\newenvironment{slide}{%
  \begin{frame}
  <presentation>\mode<presentation>{\frametitle{\insertsubsection}}%
  }%
  {\end{frame}}
\newenvironment{slide*}{\begin{frame}<presentation>[<1>]\mode<presentation>{\frametitle{\insertsubsection}}}{\end{frame}}


\beamerdefaultoverlayspecification{<+->}
%\renewcommand{\footnote}[1]{}
\usepackage[style=verbose,autocite=plain]{biblatex}
\AtEveryCite{\citereset}
\defbibheading{bibliography}{%
  \textbf{\emph{#1}}%
}
\usepackage{biblatex-true-citepages-omit}
\addbibresource{code/crossref.bib}
\addbibresource{code/MWE.bib}
\addbibresource{biblio.bib}
\setmainfont{Linux Libertine O}
\setmainlanguage{french}
\setotherlanguage{english}
\usepackage{minted}
\newcounter{code}
\resetcounteronoverlays{code}
\usepackage{hyperref}
\hypersetup{bookmarksdepth=6}
\renewcommand{\thecode}{\arabic{code}}
\newenvironment{attention}%
  {\begin{alertblock}{Attention}}%
  {\end{alertblock}}
\newenvironment{plusloin}%
  {\begin{block}{Pour aller plus loin}}%
  {\end{block}}
\newcommand{\code}[2]{%
  \stepcounter{code}%
  \begin{block}{code (\thecode)}%
			\begin{english}\footnotesize%
        \inputminted[linenos=true,breaklines=true]{#2}{code/#1}%
			\end{english}%
  \end{block}
	}
\newcommand{\alertdesc}[2]{\item[\alert{#1}]#2}
\newcommand{\meta}[1]{\texttt{<#1>}}
\newcommand{\extension}[1]{\texorpdfstring{\alert{.#1}}{.#1}}
\newcommand{\logiciel}[1]{\alert{#1}}
\newcommand{\package}[1]{\alert{#1}}
\newcommand{\BibLaTeX}{\package{Bib\LaTeX}}
\newcommand{\BibTeX}{\package{Bib\TeX}}
\newcommand{\Biber}{\package{Biber}}
\newcommand{\champ}[1]{\alert{#1}}
\newcommand{\type}[1]{\alert{@#1}}
\newcommand{\cs}[1]{\texttt{\textbackslash#1}}
\newcommand{\opt}[1]{\textbf{\texttt{#1}}}
\newcommand{\fichier}[1]{\texttt{#1}}
\setcounter{tocdepth}{1}
\AtBeginSection{
	\mode<presentation>{\frame{\sectionpage}}
}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}%remove navigation symbols

\usepackage{tikz}

\usepackage[]{qrcode}
\newcommand{\linkintitlepage}[1]{%
  \bgroup
  \tiny
  \vfill
  \parbox[c][2cm]{0.8\textwidth}{
    \url{#1}
    \vfill
    Licence Creative Commons France 3.0 - Paternité - Partage à l'identique
  }
  \hfill \qrcode{#1}
  \egroup
}

\newcommand\pointmedian{%
 \kern-0.25em\textperiodcentered\kern-0.25em}
\catcode`·=\active
\def·{\pointmedian}
\author{Maïeul Rouquette}
\date{Stage \LaTeX\ à Dunkerque}
\title{Bib\LaTeX\ et Biber}
\institute{Université de Lausanne --- IRSB}
\date{12 juin 2019}

\begin{document}


\begin{frame}
    \titlepage
    \linkintitlepage{https://geekographie.maieul.net/211}
\end{frame}

\begin{frame}
\tableofcontents
\end{frame}
% 9 partie = 10 minutes chacune
\section{Présentation des systèmes de gestion bibliographique}

\subsection{Répondre à des besoins}
\begin{slide}
\begin{itemize}

	\item Un travail scientifique utilise de nombreuses références bibliographiques.
	\item Il faut pouvoir
		\begin{itemize}
			\item Gérer / organiser pour préparer sa rédaction
			\item Citer de \alert{manière cohérente} et suivant une \alert{norme} dans son travail
			\item Lister de \alert{manière exhaustive}, selon un \alert{ordre précis} et éventuellement avec des \alert{subdivisions}
		\end{itemize}
\end{itemize}
\end{slide}

\subsection{Distinguer}
\begin{slide}

\begin{description}
	\item[Le logiciel de gestion de bibliographie] \logiciel{Zotero}, \logiciel{JabRef}, \logiciel{Endnotes}
	\item[La manière dont la bibliographie est stockée] Base de données, fichiers \extension{bib}
	\item[La manière dont elle est intégrée dans le document] commandes (\logiciel{\LaTeX}), menus (\logiciel{Words}, \logiciel{LibreOffice})
\end{description}

\end{slide}

\subsection{Dans le monde \LaTeX}
\begin{slide}

\begin{itemize}
	\item Biblio stockée dans un fichier \extension{bib}
	\item Gérée
	\begin{description}
		\item[De manière peu pratique] en écrivant soit-même le \extension{bib}
		\item[De manière pratique, directement] avec \logiciel{JabRef} (multi-OS) ; \logiciel{BibDesk} (Mac)
		\item[De manière pratique, indirectement] avec \logiciel{Zotero}
	\end{description}
	\item Intégrée selon l'une des deux méthodes suivantes
	\begin{description}
		\item[\enquote{Historique}] Bib\TeX
		\item[\enquote{Moderne}] Bib\LaTeX\ + Biber
	\end{description}
\end{itemize}
\end{slide}

\section{Bib\LaTeX, Bib\TeX\ et Biber : clarifications}
\subsection{Bib\TeX}
\begin{slide}
	\begin{itemize}
		\item Logiciel qui existe depuis 1985
		\item Souvent seul accepté par les revues qui prennent du \LaTeX
		\item De nombreux styles disponibles\ldots
		\item \ldots mais difficile à personnaliser
		\item Nécessite de nombreux packages pour s'adapter à certaines spécificités disciplinaires (par ex. citations en notes de bas de page)
	\end{itemize}
\end{slide}
\subsection{Bib\LaTeX\ + Biber}
\begin{slide}
	\begin{itemize}
		\item Package (\logiciel{\BibLaTeX}) + logiciel (\package{Biber}) récents (2009)
		\item Évolue régulièrement
		\item Souple, en un seul package on peut gérer:
		\begin{itemize}
			\item Différentes manières de faire des références bibliographiques (notes de bas de page, numéro, auteur-date etc.)
			\item Différentes manières de structurer et trier la bibliographie finale
		\end{itemize}
	 \item \enquote{Facilement} personnalisable à l'aide de commande \LaTeX
	 \item Possibilité d'automatiser l'indexation de la bibliographie
	 \item Mieux adapté à des bibliographies complexes
	\end{itemize}
\end{slide}


\subsection{Attention}
\begin{slide}
\begin{attention}
\Biber\ et \BibTeX\ utilisent tous les deux des fichiers \extension{bib}, mais ne les structurent pas de la même manière, les noms de champ pouvant varier.

Il faut donc choisir l'un ou l'autre. En général, sur internet, on trouve une structuration \BibTeX\ et pas \Biber.
\end{attention}
\end{slide}

\subsection{Quadruple compilation}

\begin{slide}
\begin{alertblock}{Attention}
Compilation toujours sur le fichier \extension{tex} et jamais sur le \extension{bib}\footnote{En réalité, avec \Biber\ on compile le fichier \extension{bcf} produit par \LaTeX. Mais les éditeurs spécialisés \LaTeX\ gèrent automatiquement le choix du fichier.}
\end{alertblock}
\begin{enumerate}
\item \LaTeX
\item \Biber
\item \LaTeX
\item \LaTeX
\end{enumerate}
\end{slide}

\begin{slide}
Nouvelle compilation
\begin{itemize}
	\item \LaTeX\ + Biber + \LaTeX\ lorsqu'on ajoute une référence dans notre travail
	\item Biber + \LaTeX\ lorsqu'on modifie une entrée bibliographique
\end{itemize}
\end{slide}

\begin{slide}
Pour simplifier la vie:
\begin{itemize}
	\item \logiciel{\TeX Studio} permet d'exécuter toutes ces compilations à la suite
	\item En ligne de commande \logiciel{latexmk} effectue toutes ces compilations à la suite
\end{itemize}
\end{slide}

\begin{slide}
	\begin{alertblock}{\TeX Shop}
	\TeX Shop (Mac) ne propose pas de compiler avec \Biber, mais on peut le configurer pour qu'il le permette.
	\end{alertblock}
\end{slide}

\section{Structure de base d'une bibliographie \protect\extension{bib}}
\subsection{Un exemple minimum}
\begin{slide*}
\begin{columns}
  \begin{column}{0.40\textwidth}
    \code{MWE.bib}{bibtex}
  \end{column}
  \begin{column}{0.60\textwidth}
    \footnotesize
    \begin{itemize}
      \item Pas de préambule
      \item Série d'\alert{entrées bibliographiques}, se définissant par:
          \begin{itemize}
            \item Un \alert{type} précédé d'un \alert{@}
            \item Une clef, après la première accolade et avant une virgule
            \item Des champs sous forme \mint{bibtex}+<nom du champ> = {<valeur>}+, séparés par des virgules
          \end{itemize}
        \item Possibilité de commenter avec \%
      \end{itemize}
    \end{column}
  \end{columns}
\end{slide*}

  \subsection{Choisir sa clef bibliographique}
  \begin{slide}
    \begin{itemize}
      \item Caractères alphanumériques non accentués + tiret médian et du bas
      \item Norme la plus simple : auteur + date
      \item Si pas de date : auteur + titre abrégé de l'œuvre
      \item Ou bien numéro dans base de données de référence (par ex.
         \enquote{BHG225})
    \end{itemize}
  \end{slide}
  \begin{slide}
  \begin{attention}
  	La clef bibliographique est sensible à la case.

	\enquote{Auzepy1995} $\neq$ \enquote{AUZEPY1995} $\neq$ \enquote{auzepy1995}
  \end{attention}
  \end{slide}
  \subsection{Les différents types d'entrées bibliographiques}
  \begin{slide*}
    Types de base
      \begin{itemize}
        \item \type{article} (comme son nom l'indique)
        \item \type{book} (livre avec un·e· ou plusieurs auteur·e·s principaux)
        \item \type{collection} (livre composé de plusieurs articles d'auteur·e·s distinct·e·s)
        \item \type{manual} (manuel)
        \item \type{reference} (ouvrage de référence, tel que dictionnaire ou encylopédie)
        \item \type{online} (ressource en ligne)
        \item \type{report} (rapport technique)
        \item \type{patent} (brevet industriel)
        \item \type{periodical} (numéro particulier d'un périodique)
        \item \type{proceedings} (actes de colloque)
        \item \type{thesis} (thèse de doctorat ou mémoire de master)
      \end{itemize}
  \end{slide*}

  \begin{slide}
    Les types \enquote{multiple}
    \begin{itemize}
      \item Dérivent des types de bases, pour indiquer lorsque plusieurs volumes existent
      \item Nom du type préfixé de \enquote{mv}
      \item Ex : \type{mvbook}, \type{mvreference}
    \end{itemize}
    \begin{attention}
      Vérifier dans le manuel de \BibLaTeX, car tous les types de base n'ont pas leur équivalent multi-volumes.
    \end{attention}
  \end{slide}
  \begin{slide}
    Les types \enquote{à l'intérieur de}
    \begin{itemize}
      \item  préfixé de \enquote{in}: \type{inbook}, \type{inreference}, \type{inproceedings}
      \item préfixé de \enquote{bookin} pour un livre édité dans un autre type.
        En standard, seulement \type{bookinbook}

      \item préfixé de \enquote{supp} pour les préfaces, avant-propos etc.
        En standard seulement \type{suppbook} et \type{suppcollection}
    \end{itemize}
  \end{slide}
  \subsection{Les différents types de champs (non exhaustifs)}
    \begin{slide*}
      Champs de personne
      \begin{description}
        \item[afterword] Auteur·trice(s) de la postface
        \item[annotator] Auteur·trice(s) des annotations
        \item[author] Auteur·trice(s) de l'œuvre
        \item[bookauthor] Auteur·trice(s) du livre dans lequel l'œuvre est insérée
        \item[commentator] Auteur·trice(s) des commentaires
        \item[editor] Éditeur·trice(s) scientifique(s). On peut en préciser le rôle grâce au champ \champ{editortype}
      \item[foreword] Auteur·trice(s) de la préface
        \item[holder] Titulaire d'un brevet industriel
        \item[introduction] Auteur·trice(s) de l'introduction
        \item[translator] Traducteur·trice(s)
      \end{description}
    \end{slide*}
    \begin{slide}
      Formatage (cas simples)
      \begin{itemize}
        \item Mot clef \enquote{and} pour séparer plusieurs auteurs
        \item \mint{bibtex}|<Nom>, <Prénom1> <Prénom2> etc.|
        \item Ou bien \mint{bibtex}|<Prénom1> <Prénom2> etc. <Nom>|
        \item Une \alert{particule} peut être insérée en minuscule avant le nom.
 Consulter le manuel de \BibLaTeX pour régler l'affichage.
         \item Pour les auteurs collectifs, utiliser des accolades.
 Ex:
           \code{CNRS.bib}{bibtex}
      \end{itemize}
      \begin{plusloin}
        Possibilité de personnaliser \BibLaTeX\ pour gérer des cas plus complexe (par ex. pour les auteurs anciens ou non occidentaux)
      \end{plusloin}
    \end{slide}
    \begin{slide*}
      Champs de titre  (liste non exhaustive)
      \begin{description}
        \item[eventitle] Titre du colloque, pour les entrées de type \type{proceedings} et \type{inproceedings}.
        \item[issuesubtitle] Sous-titre d'un numéro spécifique d'un périodique. 	Pour les entrées de type \type{periodical}, le sous-titre du périodique doit aller dans le champ \champ{subtitle}, celui du numéro dans le champ \champ{issuesubtitle}.
        \item[issuetitle] Titre d'un numéro spécifique d'un périodique. Pour les entrées de type \type{periodical}, le titre du périodique doit aller dans le champ \champ{title}, celui titre du numéro dans le champ \champ{issuetitle}.
        \item[journalsubtitle] Sous-titre d'un périodique.
        \item[journaltitle] Titre d'un périodique.
        \item[subtitle] Sous-titre de l'œuvre.
        \item[title] Titre de l'œuvre.
      \end{description}
    \end{slide*}
    \begin{slide*}
      Champs de description éditorial (liste non exhaustive)
    \begin{description}
      \item[date]
        Date de publication.
      \item[edition]
        Numéro d'édition si plusieurs éditions existent.
      \item[location] Lieu de publication.
      \item[number] Numéro d'un périodique ou numéro au sein d'une collection.
      \item[pages] Pages de l'article ou de la partie du livre étudiée.
      \item[pagetotal] Nombre total de pages.
      \item[part] Pour les livres en plusieurs volumes \emph{physiques}, indique le numéro du volume physique.  Le numéro du volume \emph{logique} est à indiquer dans le champ \champ{volume}.
      \item[publisher] Éditeur commercial.
      \item[series] Titre de collection ou de la série particulière d'un périodique.
      \item[url] Url (adresse électronique) d'une publication en ligne.
      \item[urldate] Date à laquelle une publication électronique a été consultée.
      \item[volume] Volume dans une œuvre en plusieurs volumes. Volume d'une revue.
      \item[volumes] Nombre de volumes dans une œuvres en plusieurs volumes.
    \end{description}
    \end{slide*}
    \begin{slide}
      Formatage des dates
      \code{date.bib}{bibtex}
      \begin{plusloin}
        Les dernières versions de \BibLaTeX-\Biber\ permettent de gérer des cas plus complexes (dates floues, calendriers non grégoriens etc.)
      \end{plusloin}
    \end{slide}

    \begin{slide}
      Certains champs peuvent être préfixés avec
      \begin{description}
        \item[orig] pour les informations sur l'original lors d'une rééedition / traduction
        \item[short] pour les formes courtes. Certaines sont utilisées dans les styles standards, d'autres non.
        \item[main] pour les information de l'ouvrage principal lorsqu'on désigne un volume particulier
        \item[book] pour les titres du livre lorsqu'on cite une partie de livre
      \end{description}
      \begin{attention}
        Consulter le manuel de \BibLaTeX\  pour connaître les champs ainsi prefixés définis par défaut.

        \emph{biblatex-morenames} permet d'avoir plus de champs de personnes préfixés pour les cas complexes.
      \end{attention}
    \end{slide}

    \subsection{Les références croisées}
    \begin{slide}
      \begin{itemize}
        \item Permet de dire que certains champs d'une entrée sont \alert{hérités} d'une \alert{entrée mère}
        \item Par exemple \type{mvcollection}  $\rightarrow$ \type{collection} $\rightarrow$ \type{incollection}.
        \item Les noms de champs sont automatiquement adaptés lorsqu'on passe d'une entrée à l'autre.
        \item Pour cela on utilise le champ \alert{crossref}.
      \end{itemize}
    \end{slide}

    \begin{slide}
          \code{crossref.bib}{bibtex}
    \end{slide}

    \begin{slide}
      \includegraphics[width=\textwidth]{heritage.pdf}
    \end{slide}
    \begin{slide}

          \begin{itemize}
            \item \fullcite{HistoireduChristianisme}
            \item \fullcite{Pietri1998}
            \item \fullcite{Maraval1998}
          \end{itemize}
          \begin{attention}
            \BibTeX\ possède un méchanisme de crossref, mais contrairement à \Biber:
            \begin{itemize}
                \item Un seul niveau est possible
                \item Le nom des champs n'est pas automatiquement adapté
            \end{itemize}

            \end{attention}
    \end{slide}

  \section{Logiciels de gestion bibliographique}
  \subsection{Avantages}
  \begin{slide}
    \begin{itemize}
      \item Évite les erreurs dans la composition du fichier \extension{bib}
      \item Permet de faire des recherches multicritères
      \item Permet de rechercher automatiquement dans certaines bases bibliographiques en ligne
    \end{itemize}
  \end{slide}

  \subsection{Zotero (multi-plateformes)}
    \begin{slide}
      \begin{itemize}
        \item Pas spécifique \BibTeX\ ou \BibLaTeX
        \item Utiliser Zotero-better-\BibTeX\ \url{https://github.com/retorquere/zotero-better-bibtex/wiki/Installation}
        \item Fonction de partage de références
        \item Possibilité d'automatiser les export \BibLaTeX
      \end{itemize}
      \onslide<+>{\includegraphics[width=0.8\textwidth]{zotero-better-biblatex.png}}
    \end{slide}
    \subsection{JabRef (multiplateformes)}
    \begin{slide}
      \begin{itemize}
        \item Natif \BibTeX-\BibLaTeX
        \item Dispose d'un mode spécifique \BibLaTeX
      \end{itemize}
    \end{slide}
    \begin{slide}
      \includegraphics[width=\textwidth]{jabref.png}
    \end{slide}
    \subsection{BibDesk (Mac)}
    \begin{slide}
      \begin{itemize}
        \item Natif \BibTeX
        \item Pas de mode \BibLaTeX, mais on peut aisément s'en servir pour \BibLaTeX
        \item Possibilité de créer des \enquote{groupes intelligents} croisant plusieurs critères de recherche
      \end{itemize}
    \end{slide}
    \begin{slide}
      \centering
      \includegraphics[height=0.4\textheight]{bibdesk-encodage.png}
      \vspace{\baselineskip}
      \includegraphics[height=0.4\textheight]{bibdesk-crossref.png}
    \end{slide}
\section{Charger le package, choisir ses styles}
\subsection{Charger le package, ajouter la bibliographie}
\begin{slide}
  \code{package.tex}{latex}
  \begin{itemize}
    \item Extension \extension{bib} \alert{obligatoire}
    \item Fichier peut être placé:
      \begin{itemize}
        \item À côté du fichier \extension{tex} pour un projet spécifique
        \item Dans un dossier commun : \fichier{texmfhome} $\rightarrow$ bibtex $\rightarrow$ bib
        \item Pour trouver le dossier \fichier{texmfhome}, saisir dans un terminal
          \code{texmf}{bash}
      \end{itemize}
  \end{itemize}
\end{slide}
\subsection{Deux niveaux de style}
\begin{slide}
  \begin{description}
    \item[citestyle] La manière dont est affichée une entrée lorsqu'on fait une référence
    \item[bibstyle] La manière dont est affichée la bibliographie finale
    \item[style] Combinaison des deux, qu'on utilisera en pratique
  \end{description}
\end{slide}
  \subsection{Principaux styles de citation}
\begin{slide}
  \begin{description}
    \item[numeric] les références sont des numéros renvoyant à la bibliographie finale.
    \item[alphabetic] les références sont des codes alphanumériques renvoyant à la bibliographie finale.
    \item[authortitle]seuls  l'auteur et le titre de l'œuvre sont indiqués. Plusieurs variantes existent.
    \item[authoryear]seuls l'auteur et la date de l'œuvre sont indiqués.
 Plusieurs variantes existent.
    \item[verbose]description complète la première fois, version abrégée ensuite. Plusieurs variantes existent pour avoir des abréviations latines (\emph{ibid}, \emph{idem}, \emph{op. cit.}) etc.
  \end{description}
\end{slide}
\subsection{De nombreuses autres options}
\begin{slide}
Liste non exhaustive
    \begin{description}
      \item[maxnames] Nombre maximum de noms à afficher.
      \item[indexing] Activer l'indexation des entrées bibliographiques.
      \item[abbreviate] Abréger, ou non, certains mots-clef comme \enquote{éditeur}.
      \item[autopunct] Déplacer, ou non, la référence après la ponctuation qui suit.
Pour la typographie française, mettre cette option à \alert{false}.
      \item[date] Format des dates.
      \item[giveninits] Remplace le(s) prénom(s) par son (ses) initiales.
      \item[etc] Voir le manuel, en fonction des besoins.
    \end{description}
\end{slide}
\begin{slide}
  \begin{plusloin}
    Il est possible  :
    \begin{enumerate}
      \item D'utiliser \cs{ExecuteBibliographyOptions} dans un fichier \fichier{biblatex.cfg} dans \fichier{texmfhome} pour avoir des réglages communs à plusieurs fichiers.
      \item D'utiliser \cs{ExecuteBibliographyOptions}\footnote{Dans \fichier{texmfhome} ou dans le préambule du fichier \extension{tex}.} avec un argument optionel pour appliquer ces réglages type d'entrée par  type d'entrée.
      \item Pour certaines options, de régler cela entrée par entrée dans le fichier \extension{bib} à travers le champ \champ{options}.
    \end{enumerate}
  \end{plusloin}
\end{slide}
\section{Citer}
\subsection{Commandes de base}
\begin{slide}
  \code{citation.tex}{latex}
  \onslide<2->{Mais on préférera plutôt utiliser \alert{\cs{autocite}} ou \alert{\cs{autocites}} qui}
  \begin{itemize}
    \item<3-> \enquote{Enclôt} la citation dans une commande dépendante du style:
      \begin{itemize}
        \item<4-> En note de bas de page pour le style \alert{verbose} et dérivés.
        \item<5-> Entre parenthèses pour le style \alert{authoryear}.
      \end{itemize}
    \item<6-> Adapte cette enclosure selon le contexte\footnote{\cs{autocite} dans \cs{footnote} ne produit pas une note dans une note!}.
  \end{itemize}
\end{slide}
\begin{slide}
  \begin{plusloin}
  On peut ajouter une \alert{option} \opt{autocite} au chargement du package pour régler l'\cs{autocite}:
  \begin{description}
    \item[inline] en ligne, avec parenthèses
    \item[plain] en ligne, sans parenthèses
    \item[footnote] en note de bas de page
  \end{description}
 \end{plusloin}
\end{slide}

\begin{slide}
  \begin{attention}
    En typographie française, pour ne pas avoir l'appel de note de bas de page automatiquement déplacée après le signe de ponctuation, charger \BibLaTeX\ avec l'option
    \opt{autopunct=false}.
  \end{attention}
\end{slide}

\subsection{Arguments \meta{prenote} et \meta{postnote}}
\begin{slide}
  \begin{description}
    \item[\meta{prenote}] un texte à afficher avant la référence
    \item[\meta{postnote}] un numéro de page ou un texte à afficher après la référence, ou les deux
  \end{description}

\end{slide}
\begin{slide}
  \code{prenote.tex}{latex}
  \input{code/prenote}
\end{slide}

\begin{slide}
  Formatage du numéro de page (cas simple):
  \code{page.tex}{latex}
\end{slide}
\begin{slide}
  \begin{attention}
    \BibLaTeX\ insère automatiquement \enquote{p.} ou \enquote{pp.~}
    sauf si le numéro de page est suivi d'un texte.

    Dans ce cas utiliser la commande \cs{pno} ou \cs{ppno}.

    \code{ppno.tex}{latex}
    \onslide<+->{\input{code/ppno}}
  \end{attention}
\end{slide}
\begin{slide}
  \begin{attention}
    Pour les styles \enquote{verbose} et apparentés, utiliser le package \emph{biblatex-true-citepages-omit}.

    Dans le cas contraire, lorsqu'on cite un article, \BibLaTeX\  indique comme numéro de pages:
    \begin{itemize}
      \item celui de la base de données;
      \item et celui passé en argument \meta{postnote}.
    \end{itemize}
  \end{attention}
\end{slide}
\subsection{Enclore automatiquement les citations}
\begin{slide}
  \begin{description}
    \item[\cs{footcite}] en note de bas de page
    \item[\cs{parencite}] entre parenthèses
  \end{description}
  \onslide<+->{D'autres commandes sont fournies avec certains styles}
\end{slide}


\subsection{Citer uniquement certaines informations}
\begin{slide}
  \begin{description}
    \item[\cs{citeauthor}] l'auteur (ou l'éditeur ou le traducteur si pas d'auteur)
    \item[\cs{citetitle}]  le titre ou le titre abrégé.
    \item[\cs{citetitle*}] le titre.
    \item[\cs{citeyear}] l'année.
    \item[\cs{citedate}] la date.
    \item[\cs{fullcite}] la citation complète, sans aucune abréviation.
    \item[\cs{footfullcite}] la citation complète, en notes de bas de page
    \item[\cs{nocite}] ne cite pas l'entrée, mais l'ajoute à la bibliographie finale. Si l'argument est un *, ajoute toutes les entrées de la base de données.
  \end{description}
\end{slide}



\section{Établir la bibliographie finale}

\subsection{Commande de base}

\begin{slide}
  \code{finale.tex}{latex}
  \begin{itemize}
    \item Affiche toutes les entrées citées.
    \item Affiche aussi les entrées appelées via \cs{nocite}.
    \item Affiche également les entrées citées indirectement via \alert{crossref} lorsque le nombre d'\enquote{entrées filles} citées dépasse deux\footnote{Configurable via l'option \opt{mincrossrefs} au chargement du package.}.
  \end{itemize}
\end{slide}
\subsection{Changer le tri}
\begin{slide}
  \onslide<+->{Le tri par défaut correspond au style bibliographique.

  On peut utiliser l'option \opt{sorting} \alert{au chargement du package} pour le modifier.}
  \begin{description}
    \item[nyt] Par auteur (ou éditeur), année, titre
    \item[nty] Par auteur, titre, année
    \item[ynt] Par année,  auteur, name
    \item[autres] Voir le manuel de \BibLaTeX
  \end{description}
\end{slide}
\begin{slide}
  \begin{attention}
    Si un champ n'est pas rempli, il ne compte pas dans le tri.

    Ainsi, un ouvrage sans auteur dont le titre commence par B sera classé après un ouvrage dont le nom de l'auteur commence par A\footnote{\package{bibleref-anonymous} propose des tris séparant les ouvrages anonymes des autres.}.

  \end{attention}
  \begin{plusloin}
    Il est possible de créer ses propres schémas de tri.
  \end{plusloin}
  \begin{plusloin}
    Il est possible d'avoir des tri différents si on affiche plusieurs bibliographies.
  \end{plusloin}
\end{slide}
\subsection{Filtrer}
\begin{slide}
  On peut passer des options à \cs{printbibliography} pour filtrer:
  \begin{description}
    \item[keyword] Par mot-clé enregistré dans le champ \champ{keywords}\footnote{Nom du champ au pluriel, mais nom du filtre au singulier. Possibilité de mettre plusieurs mots-clé dans le champ en utilisant une virgule comme séparateur.}.
    \item[type] Par type d'entrée.
    \item[section ou segment] Par partie du travail\footnote{Voir la documentation pour plus de détail.}.
    \item[filter / check] Par filtre maison.
    \item[autres] Voir le manuel.
  \end{description}
\end{slide}
\begin{slide}
  \begin{plusloin}
    On peut également indiquer dans le fichier bibliographique qu'une entrée ne sera pas affichée dans la bibliographie finale.

  \code{skipbib.bib}{bibtex}
  \end{plusloin}
\end{slide}
\subsection{Modifier l'entête de bibliographie}
\begin{slide}
  Options de \cs{printbibliography}
  \begin{description}
    \item[title=\meta{xxx}] Pour modifier le titre de la biblio
    \item[heading=\meta{xxx}] Pour modifier le niveau de titre de la bibliographie.
     \meta{xxx} peut être:
     \begin{description}
       \item[bibliography] (par défaut) \cs{chapter*} ou \cs{section*}
       \item[subbibliography] \cs{section*} ou \cs{subsection*}
       \item[bibintoc] \cs{chapter*} ou \cs{section*} mais ajouté à la table des matières
       \item[subbibintoc] \cs{section*} ou \cs{subsection*} mais ajouté à la tdm
       \item[bibnumbered] \cs{chapter} ou \cs{section}
       \item[subbibnumbered] \cs{subsection} ou \cs{subsection}
       \item[none] pas de titre
     \end{description}
  \end{description}
\end{slide}
\begin{slide}
  \begin{plusloin}
    \onslide<1->{Il est possible d'utiliser \cs{defbibheading} pour créer ses propres entêtes ou modifier ceux existant.}

    \onslide<+->{\code{bibheading.tex}{latex}}

  \onslide<+->{On peut également ajouter un texte entre l'entête et la bibliographie}

    \onslide<+->{\code{bibnote.tex}{latex}}
  \end{plusloin}
\end{slide}

\section{Courte introduction à la personnalisation des styles}
\subsection{Dans quels fichiers}
\begin{slide}
  \onslide{Par ordre de \enquote{propreté}}
  \begin{enumerate}
    \item Dans le préambule directement
    \item Dans un fichier \extension{tex} appelé dans chaque travail
    \item Dans un fichier \extension{sty} appelé comme package
    \item Dans le fichier \fichier{biblatex.cfg}
    \item Comme un style sous forme de fichiers \extension{bbx}, \extension{cbx}, \extension{lbx} et (éventuellement) \extension{dbx}
  \end{enumerate}
\end{slide}
\subsection{Repérer les styles utilisés}
\begin{slide}
  \onslide{Ouvrir le fichier \extension{log} pour trouver les fichiers chargés}
  \begin{description}
    \item[biblatex.def] Éléments communs à tous les styles.
    \item[\extension{cbx}] Style de citation.
    \item[\extension{bbx}] Style de bibliographie finale.
    \item[\extension{lbx}] Chaînes de langue\footcite[Voir][118-120]{Rouquette2012}.
    \item[\extension{dbx}] Champs et types autorisés\footnote{Utilisateur·trice·s avancé·e·s !}.
  \end{description}
\end{slide}
\subsection{Comprendre la structuration des styles}

\begin{slide}

  \begin{description}
    \item[Driver] Description du formatage d'un type d'entrée.
    \item[Macro] Sous description de formatage. Bien souvent correspond à une ou plusieurs unité.
    \item[Unité] Sous-division d'une référence bibliographique. Bien souvent séparée de la précédente par des signes typographiques.
    \item[Cmd. de ponctuation]Commande spécifique à Bib\LaTeX\ permettant d'afficher un signe de ponctuation entre deux unités tout en prévenant le doublement.
  \end{description}
\end{slide}

\begin{slide}
  \footnotesize\input{schemas/stylesbiblio.tex}
\end{slide}
\subsection{Commandes de haut niveau}
\begin{slide}
 \code{haut-niveau.tex}{latex}
 Consulter le manuel de \BibLaTeX\ pour connaître ces commandes de haut niveau.
\end{slide}
\subsection{Formatage des champs}
\begin{slide}
 Trois types de champ
		\begin{description}
      \item[Name]Tout champ correspondant à une personne morale ou physique
      \item[List]Tout champ pouvant contenir des valeurs séparées par \enquote{and} (sauf nom)
      \item[Field]Tout champ autre que liste et noms
 \end{description}
\end{slide}
\begin{slide}
  \code{fieldformat.tex}{latex}
\end{slide}
\begin{slide}
  \begin{attention}
    Le nom du format ne correspond pas forcément au nom du champ.
    \begin{itemize}
      \item Par exemple le format \enquote{title} s'applique à la combinaison \champ{title} + séparateur + \champ{subtitle}
      \item Il peut y avoir des alias de format
        \code{namealias.tex}{latex}
    \end{itemize}
  \end{attention}
\end{slide}

\subsection{Ordre des champs}
\begin{slide}

  Étapes de recherches
  \begin{enumerate}
    \item Repérer le driver bibliographique de notre entrée de test.
    \item Repérer la macro bibliographique correspondante au groupe de champ dont nous voulons modifier l'ordre, appelé via \cs{usebibmacro}.
    \item Repérer la définition de cette macro, définie via \cs{newbibmacro}.
    \item La modifier.
  \end{enumerate}

\end{slide}

\begin{slide}
  Exemple\footcite[Voir][122-127]{Rouquette2012}
  \begin{itemize}
    \item Je veux mettre l'éditeur avant l'adresse.
    \item Je repère que cela se passe dans la macro \texttt{publisher+location+date}.
    \item Je redéfinis la macro en conséquence.
  \end{itemize}
\end{slide}
\begin{slide}
  \code{publisher+location+date.tex}{latex}

  \begin{attention}
  Ne pas oublier les \% de fin de ligne, sinon espaces indésirables.
  \end{attention}
\end{slide}


\begin{slide}
  Le manuel décrit les nombreuses commandes de \BibLaTeX\ utilisables dans les macro/drivers permettant notamment de :
  \begin{itemize}
    \item Tester la valeur/l'existence d'un champ et modifier la présentation en conséquence.
    \item Supprimer/ajouter dynamiquement des champs.
  \end{itemize}
\end{slide}

\subsection{Créer ses styles \enquote{autonomes}}

\begin{slide}
  \begin{itemize}
    \item La manière la plus propre de créer des styles \BibLaTeX\  est d'utiliser le quadriptyques \extension{bbx}, \extension{cbx}, \extension{lbx}, \extension{dbx}.
    \item Deux solutions techniques:
      \begin{itemize}
        \item Styles totalement autonomes des styles standards  $\Rightarrow$ Reprendre les fichiers et les renommer
        \item Styles dépendants des styles standards et les personnalisant
      \end{itemize}
  \end{itemize}
\end{slide}

\begin{slide}

  \begin{itemize}
    \item Entête de fichiers
  \code{providefile.tex}{latex}
    \item Appel de style existant
  \code{RequireBibliographyStyle.tex}{latex}
  \end{itemize}
\end{slide}

\begin{slide}
  Exemple de déclaration d'option (extrait de \fichier{biblatex.sty})

  \code{DeclareOptions.tex}{latex}

\end{slide}

\section{Pour aller plus loin}

\begin{slide}
  \nocite{*}
  \printbibliography[keyword=antiseche,title=Antisèche]
  \printbibliography[keyword=initiation,title=Initiation]
  \printbibliography[keyword=manuel,title={Manuel}]
  \printbibliography[keyword=ctan,title={Styles et modules complémentaires}]

\end{slide}
\end{document}
